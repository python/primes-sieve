# Enhanced sieve of Erathostene

A very small optimized sieve to print all prime numbers under a specify parameter.

## Usage
Return a list of all prime numbers below the given parameter `n`.
```Python3
from sieve import primes

n = int(1e7)
print(primes(n))
```

## Unit testing
Complete as you wish within the `main` ; execute with:
```Bash
chmod +ux sieve.py 
./sieve.py 
```
