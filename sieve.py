#! /usr/bin/env python3
from math import sqrt

def _error(n) -> int:
    """ Print an error if n is not an integer nor is not positive
    :param n: the parameter to test.
    :return: 1 if sucess, 0 if fail
    """
    if n < 0:
        print("n = {} is not a positive number.".format(n))
        return 0
    elif not isinstance(n, int):
        print("n = {} is not an integer.".format(n))
        return 0
    return 1 

def _primes(n: int) -> list:
    """ Sieve of Eratosthene
    :param n: positive integer, upper limit to search.
    :return: a list of prime numbers under n ; a false n-long of false if n is not an positive integer.
    """
    # Argument test
    if (_error(n) != 1):
        return [-1]

    # Sieve
    sieve = []
    for i in range(2, n):
        factor = 2
        while (factor < int(sqrt(i)) + 1):
            if (i % factor != 0):
                factor += 1
            else:
                factor = i + 1
        if (factor < i + 1):
            sieve.append(i)
    return sieve

def primes(n: int) -> list:
    """ Enhanced sieve of Eratosthene
    :param n: positive integer, upper limit to search.
    :return: a list of prime numbers under n ; a false n-long of false if n is not an positive integer.
    """
    # Argument test
    if (_error(n) != 1):
        return [-1]

    # Empty list for 0, 1 or 2:
    if n in range(0, 3):
        return []

    # Sieve
    sieve = [True] * (n // 2)
    for i in range(3, int(sqrt(n)) + 1, 2):
        if sieve[i//2]:
            sieve[i*i//2::i] = [False] * ((n - i*i - 1) // (2*i) + 1)
    return [2] + [2*i+1 for i in range(1, n // 2) if sieve[i]]


if __name__ == "__main__":
    """ Testing """

    print("\n=== Normal test ===\n")
    n = int(1e3)
    print(_primes(n))
    print(primes(n))

    print("\n=== Error test ===\n")
    n = -1
    print(primes(n))
    n = 2.1
    print(primes(n))

    print("\n=== Low limit test ===\n")
    n = 0
    print(primes(n))

    print("\n=== Large limit test ===\n")
    n = int(1e7)
    print(primes(n))
